require 'destination'

RSpec.describe Destination do

  let(:destination_args) {
    {
      atlas_id: "atlas_id",
      asset_id: "asset_id",
      title: "title",
      title_ascii: "title_ascii"
    }
  }

  context '#new' do
    before(:each) do
      @destination = Destination.new(
          destination_args[:atlas_id],destination_args[:asset_id],
          destination_args[:title],destination_args[:title_ascii])
    end
    it "creates a Destination object" do
      expect(@destination).to be_an_instance_of(Destination)
    end

    it "assigns the right value to atlas_id" do
      expect(@destination.atlas_id).to eq(destination_args[:atlas_id])
    end

    it "assigns the right value to asset_id" do
      expect(@destination.asset_id).to eq(destination_args[:asset_id])
    end

    it "assigns the right value to title" do
      expect(@destination.title).to eq(destination_args[:title])
    end

    it "assigns the right value to title_ascii" do
      expect(@destination.title_ascii).to eq(destination_args[:title_ascii])
    end
  end

end