require 'nokogiri'
require 'destination_importer'
require 'destination'

RSpec.describe DestinationImporter do

  before do
    @dest1 = Destination.new("355064","22614-4","Africa","Africa")
    @dest2 = Destination.new("355611","9931-1","South Africa","South Africa")
    @dest3 = Destination.new("355612","1542-52","Cape Town","Cape Town")
    @dest4 = Destination.new("355613","","Table Mountain National Park","Table Mountain National Park")
    @dest5 = Destination.new("355616","","Gauteng","Gauteng")
    @dest6 = Destination.new("355617","1531-20","Johannesburg","Johannesburg")
    @dest7 = Destination.new("355618","","Pretoria","Pretoria")
  end

  let(:destination_xml) { Nokogiri::XML(open('spec/xml/test_destinations.xml')) }
  let(:taxonomy_xml) { Nokogiri::XML(open('spec/xml/test_taxonomy.xml')) }

  let(:destinations_list) { destination_xml.css("destination") }

  let(:imported_destinations) {
    [@dest1, @dest2, @dest3, @dest4, @dest5, @dest6, @dest7]
  }

  context '#new' do
    it "creates a DestinationImporter object" do
      destination_importer = DestinationImporter.new(destination_xml)
      expect(destination_importer).to be_an_instance_of(DestinationImporter)
    end

    it "populates the @destinations variable with the right destinations" do
      destination_importer = DestinationImporter.new(destination_xml)
      expect(destination_importer.destinations).to eq(imported_destinations)
    end
  end

  context '#parse_destination' do
    before(:each) do
      @destination_importer = DestinationImporter.new(destination_xml)
    end

    it "parses a destination XML element and returns a Destination object" do
      expect(@destination_importer.parse_destination(destinations_list[0])).to be_an_instance_of(Destination)
    end

    it "parses a destination XML element and returns the corresponding Destination object" do
      expect(@destination_importer.parse_destination(destinations_list[0])).to eq(imported_destinations[0])
    end
  end

  context '#import_taxonomy' do
    before(:each) do
      @destination_importer = DestinationImporter.new(destination_xml)
    end

    context 'recreates the right hierarchy between destinations' do
      before(:each) do
        @dest2.parent = @dest1
        @dest3.parent = @dest2
        @dest4.parent = @dest3
        @dest5.parent = @dest2
        @dest6.parent = @dest5
        @dest7.parent = @dest5

        @dest1.children = [@dest2]
        @dest2.children = [@dest3, @dest5]
        @dest3.children = [@dest4]
        @dest5.children = [@dest6, @dest7]


        @destination_importer.import_taxonomy(taxonomy_xml)
      end

      context "Africa" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[0].parent).to eq(@dest1.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[0].children).to eq(@dest1.children)
        end
      end

      context "South Africa" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[1].parent).to eq(@dest2.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[1].children).to eq(@dest2.children)
        end
      end

      context "Cape Town" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[2].parent).to eq(@dest3.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[2].children).to eq(@dest3.children)
        end
      end

      context "Table Mountain National Park" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[3].parent).to eq(@dest4.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[3].children).to eq(@dest4.children)
        end
      end

      context "Gauteng" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[4].parent).to eq(@dest5.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[4].children).to eq(@dest5.children)
        end
      end

      context "Johannesburg" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[5].parent).to eq(@dest6.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[5].children).to eq(@dest6.children)
        end
      end

      context "Pretoria" do
        it "sets the right parent" do
          expect(@destination_importer.destinations[6].parent).to eq(@dest7.parent)
        end

        it "sets its children" do
          expect(@destination_importer.destinations[6].children).to eq(@dest7.children)
        end
      end
    end
  end

end