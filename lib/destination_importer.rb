require './lib/destination'

class DestinationImporter

  INFO_MAP = {
    :history => "history/history/history",
    :introduction => "introductory/introduction/overview",
  }

  attr_reader :destinations, :destinations_map

  # accepts Nokogiri::XML::Document
  def initialize(xml_doc)
    @destinations_map = Hash.new # this adds in a kind of ActiveRecord behaviour
    @destinations = xml_doc.css("destination").map{|d| parse_destination(d)}
  end

  # accepts Nokogiri::XML::Element
  def parse_destination(destination_element)
    atlas_id = destination_element.at_xpath("@atlas_id").value
    asset_id = destination_element.at_xpath("@asset_id").value
    title = destination_element.at_xpath("@title").value
    title_ascii = destination_element.at_xpath("@title-ascii").value
    d = Destination.new(atlas_id, asset_id, title, title_ascii)
    INFO_MAP.each do |k,v|
      d.info[k] = destination_element.xpath(v).map do |h|
        h.text.gsub(/\n/,"")
      end
    end
    @destinations_map[atlas_id] = d
  end

  # accepts Nokogiri::XML::Document
  def import_taxonomy(taxonomy_xml)
    taxonomy_xml.css("node").each do |node|
      atlas_id = node.at_xpath("@atlas_node_id").value

      parent_node = node.parent
      # if the parent have the attribute 'atlas_node_id' then it's a valid parent
      if !parent_node.at_xpath("@atlas_node_id").nil?
        parent_atlas_id = parent_node.at_xpath("@atlas_node_id").value
        @destinations_map[atlas_id].parent = @destinations_map[parent_atlas_id]
        @destinations_map[atlas_id].parent.children << @destinations_map[atlas_id]
      end
    end
  end
end