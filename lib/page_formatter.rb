require 'erb'
require 'fileutils'

class PageFormatter

  ASSETS_DIR = "res/template/static"
  FILE_NAME_SUFFIX = "destination_"
  FILE_NAME_EXT = ".html"

  attr_reader :destination, :output_dir, :renderer

  def initialize(destination, output_dir)
    @destination = destination
    @output_dir = output_dir
    @renderer = ERB.new(File.read('res/template/page.html.erb'))
  end

  # this method only generates entry in the navigation panel
  # for the branch of ancestor of the selected destination
  # generating the entire hierarchy could be a 'performance nightmare'
  def generate_primary_navigation_html(destination, current_destination)
    css_class = (destination == current_destination) ? 'class="current_destination"' : ""
    output = "<a #{css_class} href=\"#{FILE_NAME_SUFFIX+destination.atlas_id+FILE_NAME_EXT}\">#{destination.title}</a>"
    unless destination.children.empty?
      if current_destination.get_ancestors.include?(destination)
        output += "<ul>"
        destination.children.each do |c|
          output +="<li>#{generate_primary_navigation_html(c,current_destination)}</li>"
        end
        output += "</ul>"
      end
    end
    output
  end

  def generate_secondary_navigation_html(destination)
    output = "<ul>"
    @destination.get_ancestors.each do |a|
      css_class = (destination == a) ? 'class="first"' : ""
      output += "<li><a href=\"#{FILE_NAME_SUFFIX+destination.atlas_id+FILE_NAME_EXT}\">#{a.title}</a></li>"
    end
    output += "</ul>"
  end

  def render
    FileUtils.mkdir(@output_dir) unless Dir.exists?(@output_dir)
    FileUtils.cp_r(ASSETS_DIR,@output_dir) unless Dir.exists?(File.join(@output_dir,ASSETS_DIR.split("/").last))
    output = @renderer.result(get_binding)
    file_path = "#{FILE_NAME_SUFFIX}#{@destination.atlas_id}#{FILE_NAME_EXT}"
    File.open(File.join(@output_dir,file_path),'w') do |f|
      f.write(output)
    end
  end

  def get_binding
    binding()
  end

end