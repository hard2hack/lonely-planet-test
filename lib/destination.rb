class Destination

  SECTIONS = {
    :introduction => "Introduction",
    :history => "History",
    # TODO: put other attributes here in a hierarchic fashion
  }

  attr_reader :atlas_id, :asset_id, :title, :title_ascii
  attr_accessor :parent, :children, :info

  def initialize(atlas_id, asset_id, title, title_ascii) # accepts Nokogiri::XML::Element
    @atlas_id = atlas_id
    @asset_id = asset_id
    @title = title
    @title_ascii = title_ascii
    @parent = nil
    @children = []
    @info = {}
  end

  def ==(other)
    self.atlas_id == other.atlas_id
  end


  def get_ancestors
    ancestors = []
    ancestors << parent.get_ancestors unless parent.nil?
    ancestors << self
    return ancestors.flatten
  end

  def get_siblings
    if parent.nil?
      [self]
    else
      parent.children
    end
  end

end