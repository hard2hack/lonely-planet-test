require 'nokogiri'
require './lib/destination_importer'
require './lib/page_formatter'

def print_usage
  puts "Usage: import.rb DESTINATION_XML_FILE TAXONOMY_XML_FILE OUTPUT_DIRECTORY"
  exit
end

# arguments validation
valid_arguments = true
if ARGV.size() < 3
  puts "Error: Too few arguments"
  print_usage
end

destinations_xml_path = ARGV[0]
taxonomy_xml_path = ARGV[1]
output_dir = ARGV[2]

unless File.exists?(destinations_xml_path)
  puts "Error: Destination file not found"
  exit
end

unless File.exists?(taxonomy_xml_path)
  puts "Error: Taxonomy file not found"
  exit
end

destinations_xml = Nokogiri::XML(open(destinations_xml_path))
taxonomy_xml = Nokogiri::XML(open(taxonomy_xml_path))

importer = DestinationImporter.new(destinations_xml)
importer.import_taxonomy(taxonomy_xml)

importer.destinations.each do |d|
  pf = PageFormatter.new(d,output_dir)
  pf.render
end